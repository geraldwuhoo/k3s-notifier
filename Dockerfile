# chef
FROM docker.io/library/rust:1.78.0 AS chef
RUN rustup target add x86_64-unknown-linux-musl && \
    apt update && \
    apt install -y libssl-dev ca-certificates musl-tools musl-dev && \
    rm -rf /var/lib/apt/lists/*
RUN cargo install cargo-chef
WORKDIR /usr/src

# planner
FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

# Builder
FROM chef AS builder
COPY --from=planner /usr/src/recipe.json recipe.json

RUN cargo chef cook --release --target x86_64-unknown-linux-musl --recipe-path recipe.json
COPY . .
RUN cargo build --release --target x86_64-unknown-linux-musl --bin k3s-notifier

# Clean image
FROM scratch
COPY --from=builder /usr/src/target/x86_64-unknown-linux-musl/release/k3s-notifier /usr/bin/k3s-notifier
COPY --from=builder /usr/lib/ssl/ /usr/local/ssl/
COPY --from=builder /etc/ssl/ /etc/ssl/
ENTRYPOINT ["k3s-notifier"]
