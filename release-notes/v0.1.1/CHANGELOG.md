# v0.1.1

* Fix channel name logging to use user-provided channel name
* Update docker image entrypoint for more natual CLI args usage
* Update Rust crate tokio to 1.33.0
* Misc CI improvements
