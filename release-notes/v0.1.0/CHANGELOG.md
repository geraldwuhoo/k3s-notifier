# v0.1.0

Initial release. Minimal working version of k3s-notifier. Currently supports following an update channel from k3s and notifying the user via ntfy.

Currently deployable via static binary and Docker image.
