# k3s update notifier

[![pipeline status](https://gitlab.wuhoo.xyz/jerry/k3s-notifier/badges/master/pipeline.svg)](https://gitlab.wuhoo.xyz/jerry/k3s-notifier/-/commits/master)

A definitely not overengineered solution to watch the [k3s updates channel](https://update.k3s.io/v1-release/channels) and notify on updates.

# Running

Static binary artifacts are available under the releases section on GitLab. [Self-hosted GitLab](https://gitlab.wuhoo.xyz/jerry/k3s-notifier/-/releases).

A docker image is available at `registry.wuhoo.xyz/jerry/k3s-notifier`.

# Usage

```
Usage: k3s-notifier [OPTIONS] --ntfy-url <NTFY_URL> --ntfy-topic <NTFY_TOPIC>

Options:
  -i, --interval <INTERVAL>
          The interval at which to check for k3s updates (minutes) [env: INTERVAL=] [default: 10]
  -n, --ntfy-url <NTFY_URL>
          ntfy URL [env: NTFY_URL=]
  -t, --ntfy-topic <NTFY_TOPIC>
          ntfy topic [env: NTFY_TOPIC=]
  -u, --ntfy-username <NTFY_USERNAME>
          ntfy username (optional) [env: NTFY_USERNAME=]
  -p, --ntfy-password <NTFY_PASSWORD>
          ntfy password (optional) [env: NTFY_PASSWORD=]
  -c, --k3s-channel-name <K3S_CHANNEL_NAME>
          The k3s channel name to watch [env: K3S_CHANNEL_NAME=] [default: stable]
  -k, --k3s-url <K3S_URL>
          k3s channels URL to watch [env: K3S_URL=] [default: https://update.k3s.io/v1-release/channels]
  -h, --help
          Print help
  -V, --version
          Print version
```

## Container

To run the container:

```
docker run --rm registry.wuhoo.xyz/jerry/k3s-notifier:latest --ntfy-url <NTFY_URL> --ntfy-topic <NTFY_TOPIC>
```
