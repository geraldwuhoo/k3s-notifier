use anyhow::{anyhow, Result};
use clap::Parser;
use ntfy::{Auth, Dispatcher, Payload, Priority};
use reqwest::Response;
use serde::{Deserialize, Serialize};
use std::{thread, time::Duration};
use url::Url;

#[derive(Serialize, Deserialize, Debug)]
struct Links {
    #[serde(rename = "self")]
    link: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Channel {
    id: String,
    links: Links,
    name: String,
    latest: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Channels {
    links: Links,
    data: Vec<Channel>,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// The interval at which to check for k3s updates (minutes)
    #[arg(short, long, env, default_value_t = 10)]
    interval: u64,

    /// ntfy URL
    #[arg(short = 'n', long, env)]
    ntfy_url: String,

    /// ntfy topic
    #[arg(short = 't', long, env)]
    ntfy_topic: String,

    /// ntfy username (optional)
    #[arg(short = 'u', long, env)]
    ntfy_username: Option<String>,

    /// ntfy password (optional)
    #[arg(short = 'p', long, env)]
    ntfy_password: Option<String>,

    /// The k3s channel name to watch
    #[arg(short = 'c', long, env, default_value = "stable")]
    k3s_channel_name: String,

    /// k3s channels URL to watch
    #[arg(
        short = 'k',
        long,
        env,
        default_value = "https://update.k3s.io/v1-release/channels"
    )]
    k3s_url: String,
}

async fn parse_response(response: Response) -> Result<Channels> {
    match response.status() {
        reqwest::StatusCode::OK => {
            let channels = response.json::<Channels>().await?;
            return Ok(channels);
        }
        other => {
            return Err(anyhow!("HTTP request returned {}", other));
        }
    }
}

async fn get_tracked_version(channel_name: &str, url: &str) -> Result<String> {
    let response = reqwest::get(Url::parse(url)?).await?;

    let channels = parse_response(response).await?;
    for channel in channels.data {
        if channel.name == channel_name {
            return Ok(channel.latest);
        }
    }

    Err(anyhow!(format!("Unable to locate {} channel in channels", channel_name)))
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    // Configure ntfy
    let mut dispatcher_builder = Dispatcher::builder(Url::parse(&args.ntfy_url)?);
    if let (Some(username), Some(password)) = (args.ntfy_username, args.ntfy_password) {
        println!("Detected username/password for ntfy");
        dispatcher_builder = dispatcher_builder.credentials(Auth::new(username, password));
    }
    let dispatcher = dispatcher_builder.build()?;
    let payload = Payload::new(&args.ntfy_topic)
        .title(format!("k3s {} updates", &args.k3s_channel_name))
        .priority(Priority::Default);

    // Grab initial current version
    let mut current_version = get_tracked_version(&args.k3s_channel_name, &args.k3s_url).await?;
    let init_msg = format!(
        "Watching for k3s updates on the {} channel.\nCurrent version: {}.\nUpdates will be checked every {} minutes.",
        &args.k3s_channel_name, current_version, &args.interval
    );
    println!("{}", init_msg);
    dispatcher
        .send(&payload.clone().message(init_msg).tags(vec!["cd", "eyes"]))
        .await?;

    loop {
        thread::sleep(Duration::from_secs(args.interval * 60));

        let version = get_tracked_version(&args.k3s_channel_name, &args.k3s_url).await?;
        println!("pulled version: {}", version);

        if current_version != version {
            let msg = format!(
                "New k3s version detected: {}\nOld k3s version: {}",
                version, current_version
            );
            println!("{}", msg);
            dispatcher
                .send(&payload.clone().message(msg).tags(vec!["cd", "arrow_down"]))
                .await?;
            current_version = version;
        }
    }
}
